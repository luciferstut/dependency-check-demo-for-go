module example/hello-world

go 1.19

require (
	github.com/valyala/fasthttp v1.6.0
	rsc.io/quote v1.5.2
)

require (
	github.com/klauspost/compress v1.15.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	golang.org/x/text v0.3.7 // indirect
	rsc.io/sampler v1.3.0 // indirect
)
